// alert('Hellow World');

/*Selection Control Structures
	- sorts out whether structure the statement/s are to be executed based on the condition whether it is true or false

	if .. else statement

	Syntax:
		If(condition) {
			//statement
		} else {
			// statement
		}

	if statement - Executes when the value is correct

	Syntax:
		if(condition) {
			// statement
		}

*/

let numA = -1;

if(numA < 0) {
	console.log('Hello');
}

console.log(numA < 0)

let city = "New York";

if(city === "New York") {
	console.log("Welcome to New York City");
}

/*
	Else if
		- executes a statement if previous conditions are false and if the specified condition is true
		- the "else if" clause is optional and can be added to capture additional conditions to change the flow of a program
*/

let numB = 1;

if (numA > 0) {
	console.log("Hello");
} else if (numB > 0){
	console.log('World')
}

//Another Example:
city = "Tokyo";

if(city === "New York") {
	console.log('Welcome to New York City!');
} else if(city === "Tokyo") {
	console.log("Welcome to Tokyo!");
}


/*
	else statement
		- executes a statement if all other condition are false

*/

if(numA > 0){
	console.log('Hello');
} else if(numB === 0){
	console.log('World');
} else {
	console.log("Again");
}


// let age = parseInt(prompt('Enter your age: '));

// if(age <= 18) {
// 	console.log('Not allowed to drink');
// } else {
// 	console.log('Matanda ka na, shot na!');
// }

/*
	Mini Activity:
		Create a conditional statement that if height is below 150, display
		"Did not passed the minimum height requirement" if above 150, display "Passed the minimum height requirement"

		** Stretch Goal: Put it inside a function
*/


let height = 160
if(height < 150){
	console.log('Passed the minimum height requirement')
} else {
	console.log('Did not passed the minimum height requirement')
}


let message = 'No message';
console.log(message)

function determineTyphoonIntensity(windSpeed) {

		if(windSpeed < 30) {
			return 'Not a typhoon yet'
		} else if (windSpeed <= 61){
			return 'Tropical depression detected'
		} else if (windSpeed >= 62 && windSpeed <= 88) {
			return 'Tropical Storm detected'
		} else if (windSpeed >= 89 && windSpeed <= 117) {
			return 'Severe Tropical storm detected'
		} else {
			return 'Typhoon Detected.'
		}
}

message = determineTyphoonIntensity(70)
console.log(message)

console.log(determineTyphoonIntensity(80))

if (message == 'Tropical Storm detected') {
	console.warn(message);
}


//Truthy and Falsy
/*
	In JS. a truthy value is a value that is considered true when encounted in a boolean context.

	Falsy Values
		1. false
		2. 0
		3. -0
		4. ""
		5. null
		6. undefined
		7. NaN
*/
// Truthy Examples
if(true) {
	console.log("Truthy")
}

if(1) {
	console.log("Truthy")
}

if ([]) {
	console.log("Truthy")
}

//Falsy Examples
if(false) {
	console.log("Falsy")
}

if(0) {
	console.log("Falsy")
}

if(undefined) {
	console.log("Falsy")
}

//Conditional (Ternary) Operator
/*
	Ternary Operator takes in three operands.
		1. condition
		2. expression to execute if the condition is truthy
		3. expression to execute if the condition is falsy

	Syntax:
		(condition) ? ifTrue_expression : ifFasle_expression
*/

//Single Statement Execution
let ternaryResult = (1 < 18) ? "Condition is True" : "Condition is False"
console.log('Result of the Ternary Operator: ' + ternaryResult)

//Multiple Statement Execution

// let name;

// function isOfLegalAge() {
// 	name = 'John'
// 	return  'You are of the legal age limit'
// }

// function isUnderAge() {
// 	name = 'Jane'
// 	return  'You are under the age limit'
// }

// let age = parseInt(prompt("What is your age? "))
// console.log(age)
// let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
// console.log('Result of Ternary Operator in Functions: '+ legalAge +', ' + name)

//Switch Statement
/*
	Can be used as an alternative to an if.. else statement where the data to be used in the condition is of an expected input

	Syntax:
		switch (expression) {
			case <value>:
				statement;
				break;

			default:
				statement;
				break
		}
*/

// let day = prompt('what day of the week is it today? ').toLowerCase()
// console.log(day);

// switch (day) {

// 	case 'monday':
// 		console.log('The color of the day is red.');
// 		break;
// 	case 'tuesday':
// 		console.log("The color of the day is orange.")
// 		break;
// 	case 'wednesday':
// 		console.log('THe color of the day is yellow');
// 		break;
// 	case 'thursday':
// 		console.log('THe color of the day is green');
// 	case 'friday':
// 		console.log('THe color of the day is blue');
// 		break;
// 	case 'saturday':
// 		console.log('THe color of the day is indigo');
// 		break;
// 	case 'sunday':
// 		console.log('THe color of the day is black');
// 		break;
// 	default:
// 		console.log('Please input a day');
// 		break;	
// }

//Try-Catch-Finally Statement
// try catch are commonly used for error handling.


function showIntensityAlert(windSpeed) {

	try {
		alertAt(determineTyphoonIntensity(windSpeed))
	}

	catch (error) {
		console.log(typeof error)
		console.warn(error.message)
	}

	finally {
		alert('Intensity updates will show new alert!!')
	}
}	

showIntensityAlert(56)